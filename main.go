// Test simple pour envoyer un message sue un port particulier

package requests

import (
    "encoding/json"
//    "fmt"
//    "net"
//    "os"
    "golang.org/x/sys/unix"
)

type Requests []Request

type Request struct {
    Action string
    Content string
}

func LoadRequest(js []byte) (Request, error) {
    var req Request
    err := json.Unmarshal(js, &req)
    if err != nil {
        return req, err
    }
    return req, nil
}

func GetSysInfo() (unix.Sysinfo_t, error) {
    var s unix.Sysinfo_t
    err := unix.Sysinfo(&s)
    if err != nil {
        return s, err
    }
    return s, nil
}

